
# Radar Pasivo Biestático  

Este repositorio contiene algunos de los resultados obtenidos en la implementación y puesta en funcionamiento de un sistema de  Radar Pasivo Biestático. 
El principal objetivo del proyecto es contar con una plataforma de prueba que permita recolectar datos y evaluar diferentes algoritmos de procesamiento, detección y seguimiento. 

Dicho trabajo fue recientemente enviado a [RPIC](https://www.rpic.com.ar/#h.wzorlo1xacgn) (Reunión de Trabajo en Procesamiento de la Información u Control) para su revisión. Puede accederse a una versión preliminar del mismo a través de este  [link](/paper/107-ISDB_T_Based_Passive_Bistatic_Radar_Testbed.pdf) o dentro del directorio "paper". 

## Introducción

Los radares convencionales son sistemas activos que operan transmitiendo energía en
forma de ondas electromagnéticas y recibiendo las señales reflejadas por la región y el objeto iluminado. Las reflexiones del ambiente, usualmente referidas como **clutter**, son consideradas como fuentes de interferencia que reducen el desempeño del sistema. 

Por su parte, los sistemas **pasivos** no requieren del uso de un transmisor dedicado. En su lugar, utilizan fuentes de energía no cooperativas denominadas iluminadores de oportunidad. Una gran cantidad de señales de radiodifusión han sido consideradas y analizadas como potenciales fuentes pasivas, siendo las de Television Digital particularmente atractivas por sus características espectrales.

En comparación con los sistemas activos, los pasivos presentan un menor costo de operación y producción, posibilitando la detección sigilosa y reduciendo la contaminación electromagnética en el entorno. Como contraparte, el limitado control sobre la fuente hacen casi nula la optimización de la forma de onda de señal en pos de mejorar la peformance del sistema. A su vez, la geometría biestática (transmisor y receptor separados) es mas compleja, y genera mayor ambigüedad y peores resoluciones.

## Principio de Funcionamiento

Las señales emitidas por el transmisor llegan a la antena receptora a través de multiples caminos. Dado que la señal de oportunidad es desconocida, el sistema estará compuesto por al menos dos antenas, una apuntando hacia la torre de transmisión y otra hacia la zona de vigilancia.



<center><img src="img/esquema_pasivo.png" width="450" height="350"/></center>

La diferencia de tiempo  entre el camino de señal directo y el reflejado se lo denomina retardo biestático ($\tau)$. Este último esta directamente relacionado con el Rango Biestático por medio de la velocidad de propagación de la luz en el espacio libre.

$$R(t) = \tau c$$

Por otro lado, la señal reflejada en un objetivo en movimiento experimenta el efecto del corrimiento Doppler ($f_d$), el cual depende de la geometría y la velocidad del mismo. Este corrimiento esta relacionado con la Velocidad Biestática por medio de: 

$$V(t)=\frac{dR(t)}{dt}=-\lambda f_d$$

donde $\lambda$ es la longitud de onda de la portadora de la señal de oportunidad.

De este modo, la detección y estimación en radar pasivo se realiza mediante una correlación entre las señales de referencia $r(t)$ y eco $s(t)$. Ésta es realizada a través de  la **Función de ambiguedad Cruzada** (CAF) resultando en una matriz 2-D denominada Mapa Rango-Doppler.
 
$$\Psi(\tau, f_d) = \int_{0}^{T_{int}} s(t) r^*(t-\tau) e^{-j 2 \pi f_d t} dt$$


## Señal de oportunidad

La señal de oportunidad utilizada corresponde a la transmsión de TV Digital del canal Universitario (TVU). El protocolo de transmisión adoptado en Argentina es el denominado ISDB-T (Integrated Services Digital Broadcasting Terrestrial). El mismo ofrece tres modos de transmisión con esquema de modulación BST-OFDM (Band Segmented Transmission-Orthogonal Frequency Division Multiplexing).  

<center><img src="img/isdbt.png" width="450" height="400" /></center>


El espectro de la señal de oportunidad esta centrado en 581.143 Mhz y presenta un ancho de banda aproximado de 6 MHz. El modo de transmisión es el número tres, con 5617 portadoras activas. Alguna de estas son pilotos empledos para la estimación del canal e información de control de transmisión. Al correlacionarse generan picos deterministicos indeseados que no puden cancelarse con el filtrado convencional y que afectan las capacidades de detección del sistema.

<center><img src="img/Figure_3.jpg" width="550" height="400" /></center>


La figura muestra la función de ambigüedad de la señal de oportunidad. El procedimiento de cálculo es similar al de la CAF, donde el
la señal del iluminador se correlaciona con una copia de si mismo desplazada en tiempo y en frecuencia Doppler. Notar que aparecen picos no deseados a partir de 25 km de rango biestático. Esto sugiere que el acondicionamiento de la señal de referencia no es estrictamente necesario para detecciones de corto alcance.

Por esto, no es necesario demodular la señal de oportunidad, y por consiguiente, conocer su estructura temporal. En definitiva, la misma será utilizada como fuente de energía electromagnética y siendo adecuadamente integrada, a través de la CAF, nos proporcionará los niveles de Relación Señal a Ruido (SNR, por sus siglas en inglés) necesarios para la detección de objetivos.


## Setup Experimental

Dado que las señales de oportunidad operan en banda UHF (300Mhz - 3Ghz), la implementación del sistema esta basada en una Radio Definida por Software (SDR, por sus siglan en Inglés) desarrollada por la empresa Ettus Research. Este dispositivo cuenta con dos cabezales de radiofrecuencia (SBX-120) que permiten contar con el ancho de banda de procesamiento adecuado.

Dado que este último es coherente, el desfasaje entre los canales de recepción es una variable de vital importancia. El fabricante informa que el mismo es aleatorio, y depende de la temperatura del sistema y de la frecuencia de sintonización  del oscilador local. Si bien ofrece una herramienta de software para mejorar este aspecto ("timed commands"),  se realizará previo a cada adquisición una calibración utilizando una señal de referencia. Ésta es generada por un subsistema compuesto de un generador de RF y un divisor Wilkinson.

<center><img src="img/setup.png" width="800" height="600" /></center>


Para la recepción se cuenta con dos antenas comerciales Yagui de 8 elementos, colocadas en la terraza del Departamento de Electrotecnia. Para mejorar la SNR se emplazo próximo a la antena de vigilancia un amplificador de bajo ruido. 
Los subsistemas antes mencionados se interconectan a través de un switch que enlaza la entradas del SDR con las antenas y las salidas del divisor. Mediante una señal de control se alterna el modo de operación entre adquisición y calibración.

<center>
<div class="container">
      <img src="img/antenna 2.png" width="400" height="400" />
      <img src="img/antennas 1.png" width="400" height="400" />
</div>
</center>


## Medición y Resultados  
Una gran cantidad de aeronaves no comerciales sobrevuelan el área vigilada. Mayormente lo hacen a baja altura y a velocidad crucero, lo cual puede constatarse en el siguiente video.

<center><video  controls src=videos/IMG-0175.MOV type="video/mp4"></center>


Aunque numerosas adquisiciones fueron realizadas hasta el momento, solo unas pocas pudieron ser contrastadas con datos de posicionamiento.
En una de las experiencias se observaron dos helicópteros Airbus H125 volando en formación a bajas altitudes cerca del Departamento de Electrotécnia. Si bien se estableció contacto visual con ellos, el sistema de localización se encontraba activó solo en el último de la fila. Esto imposibilitó verificar con precisión la separación entre los mismos. A continuación se muestra el Mapa Rango-Velocidad obtenido luego del procesamiento.
 
<center><img src="maps/RV_Map.gif" width="500" height="400" /></center>

Los picos de correlación aparecen entre 0.5 y 2 km de rango bistático, mientras que la velocidad biestática es cercana a 300 km/h (alrededor de 150 km/h en su equivalente monstática). 
Se aprecia también el efecto  del clutter estático y el desparramamiento Doppler producto del movimiento de las palas de los helicópteros (puntos verticales).

Para contrastar lo obtenido, se  calculó tanto la velocidad como el rango bistático usando datos de vuelo proporcionados por la pagina [flightradar24](https://www.flightradar24.com). Se puede observar que se corresponde con los resultados del mapa de rango-velocidad; verificando así la capacidad del sistema para detectar objetivos. 


<center><img src="img/Figure_1.jpg" width="400" height="350" /></center>


